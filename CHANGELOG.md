# Changelog

This file list changes made in each version of the this cookbook.

## Unreleased

 - Adds Debian Buster to the test suites
 - Allows changing the teleport boot timeout
 - Adds the `advertise_ip` attribute to the server config
 - Improves the code to wait for Teleport to boot
 - Uses Ruby's open-uri instead of curl to check Teleport webapi
 - Raises an error when no master node found

# 1.0.0 (2019-11-12)

 - Initial release.
