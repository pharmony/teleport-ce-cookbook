# Contributing to the Teleport Cookbook

I'm glad you'd like to contribute to this cookbook!

## Opening a Merge Request

1. Open a [new issue](https://gitlab.com/pharmony/teleport-ce-cookbook/issues/new) describing what you gonna change
2. Fork this repo
3. Write the tests and implement
4. Open a Merge Request linking to your created issue
