# frozen_string_literal: true

#
# The fields marked with a "[Mandatory]" must be field in in order to get a
# working installation.
#

# ~~~~ Teleport installation attributes ~~~~
#
# [Mandatory] Teleport version to be installed
default['teleport']['version'] = '4.1.3'
# Teleport file sha256 signature.
# Setting to nil disables the file integrity check.
default['teleport']['sha256'] = nil
# Name of the chef node where is running the Teleport server that other nodes
# will use in order to join the Teleport cluster.
# This is useful when you have multiple nodes hosting a Teleport server.
# When this value is `nil`:
#  - when you have only one node having the teleport-ce::server in its
#    `run_list`, the node will be automatically retrived
#  - when you have multiple nodes having the teleport-ce::server in their
#    `run_list`, a warning will be printed, and credential will be empty.
#    Therefore you will have to set this value accordingly.
default['teleport']['auth_server'] = nil

# ~~~~ Teleport configuration attributes ~~~~
#
## ~~~~~~~ Authentication service ~~~~~~~
#
# A cluster name is used as part of a signature in certificates
# generated by this CA.
#
# We strongly recommend to explicitly set it to something meaningful as it
# becomes important when configuring trust between multiple clusters.
#
# By default an automatically generated name is used (not recommended)
#
# IMPORTANT: if you change cluster_name, it will invalidate all generated
# certificates and keys (may need to wipe out /var/lib/teleport directory)
default['teleport']['auth_service']['cluster_name'] = 'main'
#
# IP and the port to bind to. Other Teleport nodes will be connecting to
# this port (AKA "Auth API" or "Cluster API") to validate client
# certificates
default['teleport']['auth_service']['listen_addr'] = '0.0.0.0:3025'
#
# [Mandatory] Public address (IP or DNS) of the authentication server.
# It will be used by the nodes to join the Teleport server.
default['teleport']['auth_service']['public_addr'] = 'teleport.domain.co:3025'
#
# Determines if SSH sessions to cluster nodes are forcefully terminated
# after no activity from a client (idle client).
# Examples: "30m", "1h" or "1h30m"
default['teleport']['auth_service']['client_idle_timeout'] = 'never'
#
## ~~~~~~~ SSH service ~~~~~~~
#
# IP and the port for SSH service to bind to.
default['teleport']['ssh_service']['listen_addr'] = '0.0.0.0:3022'
#
# Labeling Nodes
# In addition to specifying a custom nodename, Teleport also allows
# for the application of arbitrary key:value pairs to each node, called labels.
#
# There are two kinds of labels:
#
#  - static labels do not change over time, while teleport process is running.
#    Examples of static labels are physical location of nodes, name of
#    the environment (staging vs production), etc.
#
#  - dynamic labels also known as "label commands" allow to generate labels
#    at runtime. Teleport will execute an external command on a node
#    at a configurable frequency and the output of a command becomes
#    the label value. Examples include reporting load averages,
#    presence of a process, time after last reboot, etc.
#
# The syntax to define static labels is <label name>=<label value> like so:
#   default['teleport']['ssh_service']['labels'] = [
#     'role=master',
#     'type=postgres'
#   ]
#
# The syntax to define static labels is <label name>=[<period>:"<command>"]
# like so:
#
#   default['teleport']['ssh_service']['labels'] = [
#     'kernel=[1h:"uname -r"]',
#     'uptime=[1m:"uptime -p"]'
#   ]
#
# Both static and dynamic labels can be mixed:
#
#   default['teleport']['ssh_service']['labels'] = [
#     'kernel=[1h:"uname -r"]',
#     'role=master',
#     'type=postgres'
#     'uptime=[1m:"uptime -p"]'
#   ]
default['teleport']['ssh_service']['labels'] = []
#
## ~~~~~~~ Proxy service ~~~~~~~
#
# SSH forwarding/proxy address. Command line (CLI) clients always begin their
# SSH sessions by connecting to this port
default['teleport']['proxy_service']['listen_addr'] = '0.0.0.0:3023'
#
# Reverse tunnel listening address. An auth server (CA) can establish an
# outbound (from behind the firewall) connection to this address.
# This will allow users of the outside CA to connect to behind-the-firewall
# nodes.
default['teleport']['proxy_service']['tunnel_listen_addr'] = '0.0.0.0:3024'
#
# The HTTPS listen address to serve the Web UI and also to authenticate the
# command line (CLI) users via password+HOTP
default['teleport']['proxy_service']['web_listen_addr'] = '0.0.0.0:3080'
#
# The maximum amount of time to wait for the web server to boot before
# to raise an error
default['teleport']['proxy_service']['boot_timeout'] = 60
#
# TLS certificate for the HTTPS connection. Configuring these properly is
# critical for Teleport security.
# When nil, teleport will generate a self-signed certificate.
default['teleport']['proxy_service']['https_key_file'] = nil
default['teleport']['proxy_service']['https_cert_file'] = nil
#
## ~~~~~~~ Kubernetes service ~~~~~~~
#
# Kubernetes proxy listen address.
#
# Default value is `nil` in order to disable the kubernetes service, but the
# suggested value by Gravitationnal is '0.0.0.0:3026'
default['teleport']['kubernetes']['kube_listen_addr'] = nil

# Kubernetes proxy listen address.
#
# Default value is `nil` in order to disable the kubernetes service, but the
# suggested value by Gravitationnal is '0.0.0.0:3027'
default['teleport']['kubernetes']['listen_addr'] = nil

# The DNS name of the Kubernetes proxy server that is accessible by cluster
# clients. If running multiple proxies behind a load balancer,
# this name must point to the load balancer.
default['teleport']['kubernetes']['public_addr'] = nil

# Path to the Kubernetes configuration file
default['teleport']['kubernetes']['kubeconfig_file'] = nil

