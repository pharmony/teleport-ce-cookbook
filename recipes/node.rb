# frozen_string_literal: true

#
# Cookbook:: teleport-ce-cookbook
# Recipe:: node
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

# `fetch_auth_public_addr_from_teleport_server`
# `fetch_ca_pin_from_teleport_server`
# `fetch_static_token_from_teleport_server`
Chef::Recipe.send(:include, TeleportCookbook::NodeHelper)
# `build_dynamic_labels_for_node`
# `build_static_labels_for_node`
Chef::Resource::Template.send(:include, TeleportCookbook::Helper)

include_recipe 'teleport-ce::common'

auth_public_addr_from_server = fetch_auth_public_addr_from_teleport_server
teleport_ca_pin = fetch_ca_pin_from_teleport_server
teleport_static_token = fetch_static_token_from_teleport_server

# Installs the systemd unit for Teleport and enables it
systemd_unit 'teleport.service' do
  action %i(create enable)

  content <<-EOU.gsub(/^\s+/, '')
    [Unit]
    Description=Teleport SSH Service
    After=network.target

    [Service]
    Type=simple
    Restart=on-failure
    ExecStart=/usr/local/bin/teleport start --roles=node --config=/etc/teleport.yaml --pid-file=/var/run/teleport.pid
    ExecReload=/bin/kill -HUP $MAINPID
    PIDFile=/var/run/teleport.pid

    [Install]
    WantedBy=multi-user.target
  EOU
end

# Generates the Teleport configuration file
template '/etc/teleport.yaml' do
  action :create
  source 'node.teleport.yaml.erb'
  owner 'root'
  group 'root'
  mode '0700'
  variables(
    auth_public_addr_from_server: auth_public_addr_from_server,
    ca_pin: teleport_ca_pin,
    dynamic_labels: build_dynamic_labels_for_node,
    static_labels: build_static_labels_for_node,
    static_token: teleport_static_token
  )
end

# Finally starts the teleport service
service 'teleport' do
  action [:restart]
end
