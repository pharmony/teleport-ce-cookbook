# frozen_string_literal: true

#
# Cookbook:: teleport-ce-cookbook
# Recipe:: server
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

require 'open-uri'
require 'digest'

# `build_dynamic_labels_for_node`
# `build_static_labels_for_node`
Chef::Resource::Template.send(:include, TeleportCookbook::Helper)

require 'securerandom'

include_recipe 'teleport-ce::common'

# Installs the systemd unit for Teleport and enables it
systemd_unit 'teleport.service' do
  action %i(create enable)

  content <<-EOU.gsub(/^\s+/, '')
    [Unit]
    Description=Teleport SSH Service
    After=network.target

    [Service]
    Type=simple
    Restart=on-failure
    ExecStart=/usr/local/bin/teleport start --roles=auth,node,proxy --config=/etc/teleport.yaml --pid-file=/var/run/teleport.pid
    ExecReload=/bin/kill -HUP $MAINPID
    PIDFile=/var/run/teleport.pid

    [Install]
    WantedBy=multi-user.target
  EOU
end

# Generates and store a static token that will be used in order to allow nodes
# to join the Teleport server.
ruby_block 'generate and store a static token' do
  action  :run
  block   { node.normal['teleport']['static_token'] = SecureRandom.hex(16) }
  only_if { node['teleport']['static_token'].nil? }
end

# Generates the Teleport configuration file
template '/etc/teleport.yaml' do
  action :create
  source 'server.teleport.yaml.erb'
  owner 'root'
  group 'root'
  mode '0700'
  variables(
    dynamic_labels: build_dynamic_labels_for_node,
    static_labels: build_static_labels_for_node,
    static_token: lazy { node['teleport']['static_token'] }
  )
end

# Finally starts the teleport service
service 'teleport' do
  action [:restart]
end

ruby_block 'wait for the teleport to be up and running' do
  action :run
  block do
    web_listen_addr = node['teleport']['proxy_service']['web_listen_addr']
    web_port = if web_listen_addr && web_listen_addr.include?(':')
                 web_listen_addr.split(':').last
               else
                 '3080'
               end

    timeout = node['teleport']['proxy_service']['boot_timeout']
    Chef::Log.info "Teleport CE: Waiting for Teleport to boot on port " \
                   "#{web_port} (timeout after #{timeout} seconds) ..."

    wait_teleport_webapi = true
    webapi_url = "https://127.0.0.1:#{web_port}/webapi/ping"

    while wait_teleport_webapi
      api_output = begin
                     JSON.load(open(webapi_url,
                                    ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
                   rescue Errno::ECONNREFUSED
                     nil
                   end

      Chef::Log.info 'Teleport CE: Current Teleport webapi response: ' \
                     "#{api_output.inspect}."

      if api_output && api_output['server_version']
        Chef::Log.info 'Teleport CE: Teleport bootsted successfully'
        wait_teleport_webapi = false
      end

      timeout -= 1
      Chef::Log.info "Teleport CE: timeout: #{timeout.inspect}."
      sleep 1

      if timeout < 1
        # When the timeout is reached, the WaitCommandTimeoutError is thrown
        Chef::Log.error 'Teleport CE: Waiting for Teleport to be Running ' \
                        "failed after #{timeout} seconds."

        raise
      end
    end
  end
end

# Retrieves the CA pin that will be used by nodes to join the
# Teleport server.
ruby_block 'retrieve and store a CA pin' do
  action :run
  block do
    ca_pin = shell_out(
      "tctl status | grep \"CA pin\" | awk '{print $3}'"
    ).stdout.gsub(/\n/, '')
    Chef::Log.info "Teleport CE: Retrieved Teleport CA pin: #{ca_pin}"
    node.normal['teleport']['ca_pin'] = ca_pin
  end
end
