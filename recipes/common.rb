# frozen_string_literal: true

#
# Cookbook:: teleport-ce-cookbook
# Recipe:: common
#
# Copyright: 2019, Pharmony SA, All Rights Reserved.
#

# `teleport_archive_filename` in `log`
Chef::Resource::Log.send(:include, TeleportCookbook::Helper)

# `teleport_archive_filename` in `remote_file`
Chef::Resource::RemoteFile.send(:include, TeleportCookbook::Helper)

log 'download URL' do
  level :info
  message 'teleport-ce: Downloading Teleport binaries from ' \
          "https://get.gravitational.com/#{teleport_archive_filename}"
end

# Downloads the Teleport archive
remote_file '/tmp/teleport.tar.gz' do
  action :create
  checksum node.default['teleport']['sha256']
  mode '0755'
  source "https://get.gravitational.com/#{teleport_archive_filename}"

  only_if { shell_out('which teleport').stdout == '' }
  not_if do
    shell_out(
      "teleport version | awk '{print $2}'"
    ).stdout == "v#{node['teleport']['version']}"
  end
end

# Extract the teleport executable
ruby_block 'extract teleport' do
  action :run
  block do
    command = shell_out('tar -xvzf /tmp/teleport.tar.gz -C /tmp')
    unless command.exitstatus.zero?
      Chef::Log.error 'teleport-ce: tar command exited with ' \
                      "#{command.exitstatus}: #{command.stderr}"
    end
  end
  only_if 'test -f /tmp/teleport.tar.gz'
end

# Runs the teleport installer
ruby_block 'install teleport' do
  action :run
  block do
    command = shell_out('/tmp/teleport/install')
    unless command.exitstatus.zero?
      Chef::Log.error 'teleport-ce: teleport install command exited with ' \
                      "#{command.exitstatus}: #{command.stderr}"
    end
  end
  only_if 'test -f /tmp/teleport/install'
end

# Remove garbadge
ruby_block 'cleanup teleport' do
  action :run
  block do
    command = shell_out('rm -rf /tmp/teleport*')
    unless command.exitstatus.zero?
      Chef::Log.error 'teleport-ce: teleport cleanup command exited with ' \
                      "#{command.exitstatus}: #{command.stderr}"
    end
  end
  only_if 'test -f /tmp/teleport*'
end
