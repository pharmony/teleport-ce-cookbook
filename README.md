# Teleport CE Cookbook

Installs the [Gravitational Teleport](https://gravitational.com/teleport) Community Edition (Open source version).

This cookbook has been built following the instructions from https://gravitational.com/teleport/docs/intro/.

This cookbook supports the non-commercial features:
 - Local connector with default `otp` 2FA
 - [Labels](https://gravitational.com/teleport/docs/admin-guide/#labeling-nodes)
 - Session recording and sharing
 - SSH and Kubernetes tunneling

In the case you are looking for installing a TLS/SSL certificate, I made the [teleport-le-cookbook cookbook](https://gitlab.com/pharmony/teleport-le-cookbook) to use [Let's Encrypt](https://letsencrypt.org/) and automatically setup this cookbook in order to use the LE certificate.

High Availablity not yet implemented, to be done.

## Supported Platforms

* Debian 8/9

_If you want to add more platforms, feel free to open a PR!_

## Recipes

Downloaded archives of Teleport are from https://gravitational.com/teleport/download/.

### teleport-ce::server

Installs the teleport binaries by downloading the `-bin.tar.gz` archive for your linux distribution and architecture.

Then it writes a `/etc/teleport.yaml` file using the attributes defined in `attributes/teleport.rb` and sets [a static token](https://gravitational.com/teleport/docs/admin-guide/#static-tokens) (`tokens` attribute) used by nodes in order to join the cluster.

It also retrieves [the CA pin](https://gravitational.com/teleport/docs/admin-guide/#untrusted-auth-servers) of the server, which will be also used by the nodes in order to join the server, securing even more the connection.

It will also install a systemd unit to start teleport on node's restart, and start the service immediately.

Finally the cookbook will wait for Teleport to start by requesting its ping API.

### teleport-ce::node

Installs the teleport binaries by downloading the `-bin.tar.gz` archive for your linux distribution and architecture.

Then it writes a `/etc/teleport.yaml` file using the static token, CA pin, and the attributes defined in `attributes/teleport.rb`.

It will also install a systemd unit to start teleport on node's restart, and start the service immediately, so that it will join the node having the `teleport-ce::server` recipe in its `run_list`.

## Attributes

Please look at the `attributes/teleport.rb` file.

## Usage

In order to use this cookbook:

1. Add the cookbook to your chef repository.
2. Add the `teleport-ce::server` recipe to the `run_list` of the node where the teleport daemon shall run (only a single server supported yet).
3. Add the `teleport-ce::node` recipe to the `run_list` of the nodes that shall join the server.
4. Converge ! :)

### Policyfile example

Given a Policyfile for the server node, where you'd like to use a Let's Encrypt certificate.

```ruby
# Policyfile.rb - Describe how you want Chef to build your system.
#
# For more information on the Policyfile feature, visit
# https://docs.chef.io/policyfile.html

# A name that describes what the system you're building with Chef does.
name 'clustermaster'

# Where to find external cookbooks:
default_source :supermarket

# run_list: chef-client will run these recipes in the order specified.
run_list 'recipe[teleport-le]',
         'recipe[teleport-ce::server]'

cookbook 'teleport-ce', '~> 1.0.0'
cookbook 'teleport-le', '~> 1.0.0'

default['teleport'] = {
  auth_service: {
    cluster_name: 'staging',
    public_addr: 'kube-master.domain.co:3025'
  },
  kubernetes: {
    kubeconfig_file: '/root/.kube/config'
  },
  sha256: 'ed201e7a84ccfa7d32f77ef5626ed7f1c38bd0f6fd629c787d9c1731177504e3',
  ssh_service: {
    labels: [
      'environment=dev',
      'role=master',
      'type=kubernetes'
    ]
  },
  version: '4.1.1'
}
```

And a Policyfile for a node of your cluster.

```ruby
# Policyfile.rb - Describe how you want Chef to build your system.
#
# For more information on the Policyfile feature, visit
# https://docs.chef.io/policyfile.html

# A name that describes what the system you're building with Chef does.
name 'clusterworker'

# Where to find external cookbooks:
default_source :supermarket

# run_list: chef-client will run these recipes in the order specified.
run_list 'recipe[teleport-ce::node]'

cookbook 'teleport-ce', '~> 1.0.0'

default['teleport'] = {
  auth_server: 'kube-master',
  sha256: 'ed201e7a84ccfa7d32f77ef5626ed7f1c38bd0f6fd629c787d9c1731177504e3',
  ssh_service: {
    labels: [
      'environment=dev',
      'role=worker',
      'type=kubernetes'
    ]
  },
  version: '4.1.1'
}
```

Using the `bootstrap` command you can assign the right policy to the right nodes.

When the master node is ready, you should be able to connect to its FQDN on port `3080` (or the port defined from `default['teleport']['proxy_service']['web_listen_addr']` from the `attributes/teleport.rb` file) and get the teleport login page.

You now have to [create an invitation link](https://gravitational.com/teleport/docs/admin-guide/#adding-and-deleting-users) to yourself and all the people allowed to connect to the node of this Teleport cluster.

### Kubernetes integration

You have to use the `teleport-ce::server` recipe on a node which can connect to the Kubernetes cluster, meaning a node which can do `kubectl get nodes`.

