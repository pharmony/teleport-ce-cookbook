# frozen_string_literal: true

# Inspec test for recipe teleport-ce-cookbook::node

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# The teleport file should be present
describe file('/usr/local/bin/teleport') do
  it { should exist }
  it { should be_executable }
end

# The teleport systemd unit should be present, the service enabled and running
describe systemd_service('teleport') do
  it { should be_installed }
  it { should be_enabled }
  # We can't test if the service is running as it tries to connect to the given
  # Teleport server, which doesn't exist.
  # it { should be_running }
end
# Ensures the systemd unit is a "server" kind, having all the roles.
describe file('/etc/systemd/system/teleport.service') do
  its('content') do
    should match(
      %r{ExecStart=/usr/local/bin/teleport start --roles=node --config=/etc/teleport.yaml.*}
    )
  end
end

# The teleport configuration file should be present
describe file('/etc/teleport.yaml') do
  it { should exist }
  it { should be_file }

  its('content') do
    should match(/nodename: teleport-node.gravitational.vagrant/)
  end
  its('content') do
    should match(/auth_token: fd265fa3e4a8e3bbeffd3bcc7293b743/)
  end
  its('content') do
    should match(/ca_pin: "sha256:d3d2b5bec89c1dd8ac557e08221fb17d6087cf5e18c646e9a92976cc1a28a3bb"/)
  end
  its('content') do
    should match(/auth_servers:\n\s+- teleport-server.gravitational.vagrant:3025/)
  end
  its('content') do
    should match(/labels:\n\s+role: slave\n\s+type: postgres/)
  end
  its('content') do
    should match(/commands:\n\s+- name: kernel\n\s+command: \["uname", "-r", "-s"\]\n\s+period: 1h0m0s\n\s+- name: uptime\n\s+command: \["uptime", "-p"\]\n\s+period: 0h1m0s/)
  end
end

# Closed/Opened ports can't be tested without teleport being running :(
