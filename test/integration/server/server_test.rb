# frozen_string_literal: true

# Inspec test for recipe teleport-ce-cookbook::server

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# The teleport file should be present
describe file('/usr/local/bin/teleport') do
  it { should exist }
  it { should be_executable }
end

# The teleport systemd unit should be present, the service enabled and running
describe systemd_service('teleport') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# Ensures the systemd unit is a "server" kind, having all the roles.
describe file('/etc/systemd/system/teleport.service') do
  its('content') do
    should match(%r{ExecStart=/usr/local/bin/teleport start --roles=auth,node,proxy --config=/etc/teleport.yaml.*})
  end
end

# The teleport configuration file should be present
describe file('/etc/teleport.yaml') do
  it { should exist }
  it { should be_file }

  its('content') do
    should match(/nodename: teleport-server.gravitational.vagrant/)
  end
  its('content') { should match(/advertise_ip: [\d\.]+/) }
  its('content') { should match(/tokens:\n\s+- "node:[\w]{32,}"/) }
  its('content') do
    should match(/labels:\n\s+role: master\n\s+type: postgres/)
  end
  its('content') do
    should match(/commands:\n\s+- name: kernel\n\s+command: \["uname", "-r"\]\n\s+period: 1h0m0s\n\s+- name: uptime\n\s+command: \["uptime", "-p"\]\n\s+period: 0h1m0s/)
  end
end

# Auth access open
describe port(3025) do
  it { should be_listening }
  its('processes') { should include 'teleport' }
  its('protocols') { should include 'tcp' }
end

# SSH access open
describe port(3022) do
  it { should be_listening }
  its('processes') { should include 'teleport' }
  its('protocols') { should include 'tcp' }
end

# Proxy SSH access open
describe port(3023) do
  it { should be_listening }
  its('processes') { should include 'teleport' }
  its('protocols') { should include 'tcp' }
end

# Proxy reverse SSH tunnels access open
describe port(3024) do
  it { should be_listening }
  its('processes') { should include 'teleport' }
  its('protocols') { should include 'tcp' }
end

# Proxy web access open
describe port(3080) do
  it { should be_listening }
  its('processes') { should include 'teleport' }
  its('protocols') { should include 'tcp' }
end

# Kubernetes access closed
describe port(3026) do
  it { should_not be_listening }
end
