# frozen_string_literal: true

module TeleportCookbook
  module NodeHelper
    include Chef::Mixin::ShellOut

    def fetch_auth_public_addr_from_teleport_server
      server = retrieve_server_node

      return nil unless server

      server.default['teleport']['auth_service']['public_addr']
    end

    def fetch_ca_pin_from_teleport_server
      server = retrieve_server_node

      return nil unless server

      server.normal['teleport']['ca_pin']
    end

    def fetch_static_token_from_teleport_server
      server = retrieve_server_node

      return nil unless server

      server.normal['teleport']['static_token']
    end

    private

    def find_server_from_multiple_servers(servers)
      unless node['teleport']['auth_server']
        Chef::Log.warn "Teleport CE: WARNING: There are #{servers.size} " \
                       'Teleport servers in your Chef repository. ' \
                       "This cookbook doesn't support this use " \
                       'case therefore the Teleport config file will miss ' \
                       'some critical values from the Teleport server.'
        return nil
      end

      Chef::Log.warn 'Teleport CE: Searching the auth server ' \
                     "#{node['teleport']['auth_server']} from " \
                     "#{servers.size} servers ..."
      target_server = servers.select do |server|
        Chef::Log.warn "Teleport CE: Checking #{server.name} ..."
        server.name == node['teleport']['auth_server']
      end.first

      if target_server
        Chef::Log.warn 'Teleport CE: Server ' \
                       "#{node['teleport']['auth_server']} found!"
        return target_server
      end

      Chef::Log.error 'Teleport CE: ERROR: Unable to find the node with name ' \
                      "#{node['teleport']['auth_server']} in your chef " \
                      'repository.'

      raise
    end

    def retrieve_server_node
      servers = search(:node, 'run_list:*teleport-ce??server*')

      # No existing servers with the teleport-ce::server recipe found
      if servers.size.zero?
        Chef::Log.warn 'Teleport CE: WARNING: Your chef repository has no ' \
                       'node having the teleport-ce::server recipe ' \
                       'therefore the Teleport config file will miss some ' \
                       'critical values from the Teleport server.'
        return nil
      end

      return find_server_from_multiple_servers(servers) if servers.size > 1

      servers.first
    end
  end
end
