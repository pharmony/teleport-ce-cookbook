# frozen_string_literal: true

module TeleportCookbook
  module Helper
    include Chef::Mixin::ShellOut

    #
    # Returns an Array of all the defined dynamic labels.
    #
    # For example, in the case the `node['teleport']['ssh_service']['labels']`
    # is made like this:
    #
    # ```
    # node['teleport']['ssh_service']['labels'] = [
    #   'kernel=[1h:"uname -r"]',
    #   'uptime=[1m:"uptime -p"]'
    # ]
    # ```
    #
    # it will return the following:
    #
    # ```
    # [
    #   { key: 'kernel', period: '1h0m0s', value: '["uname", "-r"]'},
    #   { key: 'uptime', period: '0h1m0s', value: '["uptime", "-p"]'}
    # ]
    # ```
    #
    def build_dynamic_labels_for_node
      filtered_labels = filter_dynamic_labels

      filtered_labels.map do |label|
        key, period_command = label.split('=')

        # Removes the `[]` and split on `:`
        period, command = period_command[1..-2].split(':')

        {
          key: key,
          period: build_period_from(period),
          # Removes the surrounding double quotes and build the command
          value: build_command_from(command[1..-2])
        }
      end
    end

    #
    # Returns an Array of all the defined static labels.
    #
    # For example, in the case the `node['teleport']['ssh_service']['labels']`
    # is made like this:
    #
    # ```
    # node['teleport']['ssh_service']['labels'] = [
    #   'role=master',
    #   'type=postgres'
    # ]
    # ```
    #
    # it will return the following:
    #
    # ```
    # [{ key: 'role', value: 'master'}, { key: 'type', value: 'postgres'}]
    # ```
    #
    def build_static_labels_for_node
      filter_static_labels.map do |label|
        key, value = label.split('=')

        { key: key, value: value }
      end
    end

    #
    # Returns the complete teleport achive name for the node's platform arch and
    # name.
    #
    def teleport_archive_filename
      [
        'teleport',
        "v#{version}",
        platform_name,
        platform_arch,
        'bin.tar.gz'
      ].join('-')
    end

    private

    def build_command_from(command)
      app, *args = command.split(' -')

      final_command = "[\"#{app}\"".dup

      args.each { |arg| final_command << ", \"-#{arg}\"" }

      final_command << ']'
    end

    #
    # Build the final period string from the given period.
    #
    # For example:
    #
    #   - when period is equal to "1m", this method return "0h1m0s"
    #   - when period is equal to "1h", this method return "1h0m0s"
    #   - when period is equal to "1h20m", this method return "1h20m0s"
    #   - when period is equal to "1h20h", this method return "20h0m0s"
    #
    # From the Teleport doc: this value cannot be less than one minute, so :
    #
    #   - when period is equal to "1s", this method return "0h1m0s"
    #
    def build_period_from(period)
      hours = 0
      minutes = 0
      period.scan(/(\d+)(\w)/).each do |duration, unit|
        case unit
        when 'h' then hours = duration
        when 'm' then minutes = duration
        end
      end

      minutes = 1 if hours == 0

      "#{hours}h#{minutes}m0s"
    end

    def filter_dynamic_labels
      labels = node['teleport']['ssh_service']['labels']

      labels.select { |label| label =~ /^\w+=\[.*\]$/ }
    end

    def filter_static_labels
      labels = node['teleport']['ssh_service']['labels']

      labels.select { |label| (label =~ /^\w+=\[.*\]$/).nil? }
    end

    def platform_arch
      case run_command('uname -m')
      when 'x86_64' then 'amd64'
      end
    end

    def platform_name
      run_command('uname -s').downcase
    end

    def run_command(command)
      shell_out(command).stdout.gsub(/\n/, '')
    end

    def version
      node['teleport']['version']
    end
  end
end
