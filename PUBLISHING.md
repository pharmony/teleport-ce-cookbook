# Publishing a new release

This project uses the `stove` gem to publish new releases to the Chef supermarket.

1. Update the `CHANGELOG.md` file in order to move all the items from the **Unreleased** into a new version (See existing versions in the `CHANGELOG.md` file).
2. Update the `metadata.rb` file `version` attribute.
3. Commit the changes with the commit message `Bumped version v<version>`
4. Create a git tag `v<version>` on this commit and push it
5. Wait for the build and if it pass
6. Install `stove` if needed
    ```
    $ gem install stove
    ```
7. Login with `stove`
    ```
    $ stove login --username <your-username> --key ~/.chef/<your-username>.pem
    ```
8. Publish the 
    ```
    $ stove --no-git
    ```
