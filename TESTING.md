# Testing the Teleport CE cookbook

This cookbook uses the Kitchen CI in order to run the tests.
All the configuration can be found in the `.kitchen.yml` (Dokken driver), `.kitchen.docker.yml` (Docker driver), and `.kitchen.vagrant` (Vagrant driver) files.

## First time

You need to install the dependencies in order to get kitchen drivers availables.

From this repo's folder:

```
$ bundle
```

## Usage

### Integration tests

You can list the Kitchens :

```
$ bundle exec kitchen list
Instance          Driver  Provisioner  Verifier  Transport  Last Action    Last Error
server-debian-8  Dokken  Dokken       Inspec    Dokken     <Not Created>  <None>
server-debian-9  Dokken  Dokken       Inspec    Dokken     <Not Created>  <None>
```

To run the tests, simply run `bundle exec kitchen test` :

```
$ bundle exec kitchen test
-----> Starting Kitchen (v1.24.0)
-----> Cleaning up any prior instances of <server-debian-9>
-----> Destroying <server-debian-9>...
...
-----> Verifying <server-debian-9>...
       Loaded tests from {:path=>".Users.zedtux.Developments.teleport-ce-cookbook.test.integration.server"}

Profile: tests from {:path=>"/Users/zedtux/Developments/teleport-ce-cookbook/test/integration/server"} (tests from {:path=>".Users.zedtux.Developments.teleport-ce-cookbook.test.integration.server"})
Version: (not specified)
Target:  docker://22a28cca31bb89f5b5051d7f4adb0084288113e18b181c7bd2d60eaacf14a407

  File /usr/local/bin/teleport
     ✔  should exist
     ✔  should be executable
  Service teleport
     ✔  should be installed
     ✔  should be enabled
     ✔  should be running
  File /etc/teleport.yaml
     ✔  should exist
     ✔  should be file
     ✔  content should match /nodename: teleport-server/
     ✔  content should match /tokens:\n\s+- "node:[\w]+"/
  Port 3025
     ✔  should be listening
     ✔  processes should include "teleport"
     ✔  protocols should include "tcp"
  Port 3022
     ✔  should be listening
     ✔  processes should include "teleport"
     ✔  protocols should include "tcp"
  Port 3023
     ✔  should be listening
     ✔  processes should include "teleport"
     ✔  protocols should include "tcp"
  Port 3024
     ✔  should be listening
     ✔  processes should include "teleport"
     ✔  protocols should include "tcp"
  Port 3080
     ✔  should be listening
     ✔  processes should include "teleport"
     ✔  protocols should include "tcp"

Test Summary: 24 successful, 0 failures, 0 skipped
       Finished verifying <server-debian-9> (0m5.39s).
-----> Destroying <server-debian-9>...
       Deleting kitchen sandbox at /Users/zedtux/.dokken/kitchen_sandbox/358b85956f-server-debian-9
       Deleting verifier sandbox at /Users/zedtux/.dokken/verifier_sandbox/358b85956f-server-debian-9
       Finished destroying <server-debian-9> (0m11.68s).
       Finished testing <server-debian-9> (2m28.62s).
-----> Kitchen is finished. (2m32.58s)
```

### Testing with the Docker driver

All you need to do is to prefix the above commands with `KITCHEN_YAML=.kitchen.docker.yml` :

```
$ KITCHEN_YAML=.kitchen.docker.yml bundle exec kitchen list
Instance          Driver  Provisioner  Verifier  Transport  Last Action    Last Error
server-debian-8  Docker  ChefZero     Inspec    Ssh        <Not Created>  <None>
server-debian-9  Docker  ChefZero     Inspec    Ssh        <Not Created>  <None>
```

And the same for running tests :

```
$ KITCHEN_YAML=.kitchen.docker.yml bundle exec kitchen test
...
```
