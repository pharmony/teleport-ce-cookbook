# By default, this file should be stored in /etc/teleport.yaml
# See https://gravitational.com/teleport/docs/admin-guide/#using-teleport-with-openssh

## IMPORTANT ##
# When editing YAML configuration, please pay attention to how your editor
# handles white space. YAML requires consistent handling of tab characters.

# This section of the configuration file applies to all teleport
# services.
teleport:
  # nodename allows to assign an alternative name this node can be reached by.
  # by default it's equal to hostname
  nodename: <%= node['fqdn'] %>

  # Data directory where Teleport keeps its data, like keys/users for
  # authentication (if using the default BoltDB back-end)
  data_dir: /var/lib/teleport

  # When running in multi-homed or NATed environments Teleport nodes need
  # to know which IP it will be reachable at by other nodes
  #
  # This value can be specified as FQDN e.g. host.example.com
  advertise_ip: <%= node['ipaddress'] %>

  # Teleport throttles all connections to avoid abuse. These settings allow
  # you to adjust the default limits
  connection_limits:
    max_connections: 1000
    max_users: 250

  # Logging configuration. Possible output values are 'stdout', 'stderr' and
  # 'syslog'. Possible severity values are INFO, WARN and ERROR (default).
  log:
    output: stderr
    severity: INFO

  # Type of storage used for keys. You need to configure this to use etcd
  # backend if you want to run Teleport in HA configuration.
  storage:
    type: dir
    path: /var/lib/teleport

# This section configures the 'auth service':
auth_service:
  # Turns 'auth' role on. Default is 'yes'
  enabled: yes

  # A cluster name is used as part of a signature in certificates
  # generated by this CA.
  #
  # We strongly recommend to explicitly set it to something meaningful as it
  # becomes important when configuring trust between multiple clusters.
  #
  # By default an automatically generated name is used (not recommended)
  #
  # IMPORTANT: if you change cluster_name, it will invalidate all generated
  # certificates and keys (may need to wipe out /var/lib/teleport directory)
  cluster_name: "<%= node['teleport']['auth_service']['cluster_name'] %>"

  authentication:
    # With the non-commercial version of Teleport, only the local
    # authentication method is available.
    type: local

  # IP and the port to bind to. Other Teleport nodes will be connecting to
  # this port (AKA "Auth API" or "Cluster API") to validate client
  # certificates
  listen_addr: <%= node['teleport']['auth_service']['listen_addr'] %>

  # The optional DNS name the auth server if located behind a load balancer.
  # (see public_addr section below)
  public_addr: <%= node['teleport']['auth_service']['public_addr'] %>

  # Pre-defined tokens for adding new nodes to a cluster. Each token specifies
  # the role a new node will be allowed to assume. The more secure way to
  # add nodes is to use `ttl node add --ttl` command to generate auto-expiring
  # tokens.
  #
  # We recommend to use tools like `pwgen` to generate sufficiently random
  # tokens of 32+ byte length.
  tokens:
    - "node:<%= @static_token %>"

  # Determines if SSH sessions to cluster nodes are forcefully terminated
  # after no activity from a client (idle client).
  # Examples: "30m", "1h" or "1h30m"
  client_idle_timeout: <%= node['teleport']['auth_service']['client_idle_timeout'] %>

# This section configures the 'node service':
ssh_service:
  # Turns 'ssh' role on. Default is 'yes'
  enabled: yes

  <% if @dynamic_labels.size > 0 %>
  # Dynamic labels AKA "commands":
  # The period setting tells teleport to execute the command above
  # once an hour. This value cannot be less than one minute.
  commands:
    <% @dynamic_labels.sort_by { |label| label[:key] }.each do |label| %>
    - name: <%= label[:key] %>
      command: <%= label[:value] %>
      period: <%= label[:period] %>
    <% end %>
  <% end %>

  <% if @static_labels.size > 0 %>
  # In addition to specifying a custom nodename, Teleport also allows for
  # the application of arbitrary key:value pairs to each node, called labels.
  #
  # Here are defined static labels.
  #
  # See https://gravitational.com/teleport/docs/admin-guide/#labeling-nodes
  labels:
    <% # Print the labels sorted by label key %>
    <% @static_labels.sort_by { |label| label[:key] }.each do |label| %>
    <%= label[:key] %>: <%= label[:value] %>
    <% end %>
  <% end %>

  # IP and the port for SSH service to bind to.
  listen_addr: <%= node['teleport']['ssh_service']['listen_addr'] %>

# This section configures the 'proxy service'
proxy_service:
  # Turns 'proxy' role on. Default is 'yes'
  enabled: yes

  # SSH forwarding/proxy address. Command line (CLI) clients always begin their
  # SSH sessions by connecting to this port
  listen_addr: <%= node['teleport']['proxy_service']['listen_addr'] %>

  # Reverse tunnel listening address. An auth server (CA) can establish an
  # outbound (from behind the firewall) connection to this address.
  # This will allow users of the outside CA to connect to behind-the-firewall
  # nodes.
  tunnel_listen_addr: <%= node['teleport']['proxy_service']['tunnel_listen_addr'] %>

  # The HTTPS listen address to serve the Web UI and also to authenticate the
  # command line (CLI) users via password+HOTP
  web_listen_addr: <%= node['teleport']['proxy_service']['web_listen_addr'] %>

  <% if node['teleport']['kubernetes']['kube_listen_addr'] %>
  # Kubernetes proxy listen address.
  kube_listen_addr: <%= node['teleport']['kubernetes']['kube_listen_addr'] %>

  # The DNS name of the Kubernetes proxy server that is accessible by cluster
  # clients. If running multiple proxies behind  a load balancer, this name
  # must point to the load balancer.
  public_addr: [
  <% if node['teleport']['kubernetes']['public_addr'].nil? %>
    '<%=
      node['teleport']['auth_service']['public_addr'].split(':').first
    %>:<%=
      node['teleport']['kubernetes']['kube_listen_addr'].split(':').last
    %>'
  <% else %>
    <% node['teleport']['kubernetes']['public_addr'].each do |address| %>
    <%= address %>,
    <% end %>
  <% end %>
  ]
  <% end %>

  # TLS certificate for the HTTPS connection. Configuring these properly is
  # critical for Teleport security.
  https_keypairs:
    - key_file: <%= node['teleport']['proxy_service']['https_key_file'] %>
      cert_file: <%= node['teleport']['proxy_service']['https_cert_file'] %>

<% if node['teleport']['kubernetes']['listen_addr'] %>
# This section configures the 'proxy service'
kubernetes_service:
  # Turns 'kubernetes' role on. Default is 'yes'
  enabled: yes

  # Kubernetes forwarding/proxy address.
  listen_addr: <%= node['teleport']['kubernetes']['listen_addr'] %>

  <% if node['teleport']['kubernetes']['kubeconfig_file'] %>
  # This setting is not required if the Teleport proxy service is
  # deployed inside a Kubernetes cluster. Otherwise, Teleport proxy
  # will use the credentials from this file:
  kubeconfig_file: <%= node['teleport']['kubernetes']['kubeconfig_file'] %>
  <% end %>
<% end %>
